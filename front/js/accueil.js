//Déclaration de la fonction pour récupérer le tableau d'objet
/**
 * Récupération des données depuis l'API en utilisant la méthode fetch 
 * @return {promise}
 * @return {object[] | error}
 */
const listOfAllArticles = async () => {
    await fetch(" http://localhost:3000/api/products/")
        .then(function (res) {
            if (res.ok) {
                return res.json()
            }
        })
        .then(function (value) {
            listOfKanap = value;
            console.log(value);
        })
        .catch(function (err) {
            console.log('erreur')
        })
}
listOfAllArticles();


/**
 * Intégration des données depuis l'API dans la page d'accueil en utilisant une boucle for/in
 * @param {Object[]} kanap
 * @param {String} kanap[].imageUrl
 * @param {Integer} kanap[]._id
 * @param {String} kanap[].altText
 * @param {String} kanap[].name
 * @param {String} kanap[].description
 * @param {function()} callback
 */

//Déclaration de la fonction pour effectuer les boucles sur la base du tableau d'objets récupéré ci-dessus
const detailKanap = async () => {
    await listOfAllArticles();
    //boucle for sur les 8 objets qui contient toutes les informations sur chaque article
    for (let kanap of listOfKanap) {
        //Cible de la section dans laquelle seront mises les informations sur l'article
        let eltSection = document.getElementById("items");
        //Création d'un a href
        let eltLinkArticle = document.createElement('a');
        //Rattachement du lien créé au DOM au niveau de la section ayant pour identifiant 'items'
        eltSection.appendChild(eltLinkArticle);
        //Création d'une balise <article></article>
        let eltArticle = document.createElement('article');
        //Rattachement de la balise <article></article> au DOM au niveau du lien
        eltLinkArticle.appendChild(eltArticle);
        //Création de l'élément img
        let eltImage = document.createElement('img');
        //Rattachement au DOM au niveau de la balise <article></article>
        eltArticle.appendChild(eltImage);
        //Création de l'élément <p>
        let eltParag = document.createElement('p');
        //Rajout de la classe productDescription à la balise <p>
        eltParag.classList.add("productDescription");
        //Rattachement au DOM au niveau de la balise <article></article>
        eltArticle.appendChild(eltParag);
        //Création du titre de niveau 3
        let eltTitle = document.createElement('h3');
        //Rajour de la classe productName au titre de niveau 3 h3
        eltTitle.classList.add("productName");
        //Rattachement au DOM au niveau de la balise <article></article>
        eltArticle.appendChild(eltTitle);

        //Modification de l'attribue href du lien a par le chemin pour accéder au détail de l'article avec son identifiant
        eltLinkArticle.setAttribute(`href`, `../html/product.html?id=${kanap._id}`);
        //Modification de l'attribue src par les données        
        let srcImage = `${kanap.imageUrl}`;
        //Modification de l'attribue alt par les données
        let altImage = `${kanap.altTxt}`;
        //Rajout de l'attribue src à la balise <img>
        eltImage.src += srcImage;
        //Rajout de l'attribue alt à la balise <img>
        eltImage.alt += altImage;
        //Inialisation de la variable par les données de nom de l'article
        eltTitle.textContent += `${kanap.name} `;
        //inialisation de la variable par les données de détails de l'article
        eltParag.textContent += `${kanap.description} `;

    }

}

detailKanap();

