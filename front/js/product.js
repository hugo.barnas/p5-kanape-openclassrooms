let listOfKanap;
let search_params;
let url;
let urlPageArticle;

/**
 * Intégration des données depuis l'API dans la page d'accueil en utilisant une boucle for/in
 * @param {Object[]} kanap
 * @param {String} kanap[].imageUrl
 * @param {Integer} kanap[]._id
 * @param {String} kanap[].altText
 * @param {String} kanap[].name
 * @param {String} kanap[].description
 * @param {function()} callback
 */

const listOfAllArticles = async () => {
    await fetch(" http://localhost:3000/api/products/")
        .then(function (res) {
            if (res.ok) {
                return res.json()
            }
        })
        .then(function (value) {
            listOfKanap = value;
        })
        .catch(function (err) {
            console.log('erreur')
        })
};

/**
 * Récupérer l’URL du page courante
 *  @param {String} http
 */
const urlKanap = () => {
    urlPageArticle = window.location.href;
    url = new URL(urlPageArticle);
    search_params = new URLSearchParams(url.search);
}

/**
 * Récupérer l'id de l'article correspondant à l'id récupérer dans l'url
 * @param {integer} kanap[]._id
 * @param {String} kanap[].altText
 * @param {String} kanap[].imageUrl
 * comparer l'id de l'article avec l'id de l'adresse url
 * Récupérer la source de l'image de l'id de l'article correspondant
 * Intégrer cette image dans le DOM
 * Intéger l'attribut alt de cette image
 */

const imagArticle = async () => {
    if (search_params.has('id')) {
        await listOfAllArticles();
        for (let kanap of listOfKanap) {
            let idList = kanap._id;
            let idArticle = search_params.get('id');
            if (idArticle === idList) {
                let eltImage = document.createElement('img');
                let divImage = document.querySelector(".item__img");
                divImage.appendChild(eltImage);
                let srcImage = `${kanap.imageUrl}`;
                //Modification de l'attribue alt par les données
                let altImage = `${kanap.altTxt}`;
                //Rajout de l'attribue src à la balise <img>
                eltImage.src += srcImage;
                //Rajout de l'attribue alt à la balise <img>
                eltImage.alt += altImage;

            } else {
                console.log(false);
            }
        }
    }
}

/**
 * Récupérer l'image de l'article correspondant à l'id récupérer dans l'url
 * @param {} kanap[].name
 * comparer l'id de l'article avec l'id de l'adresse url
 * Récupérer le nom de l'article de l'id de l'article correspondant
 * Intégrer le nom de l'article dans le DOM
 */

const nameArticle = async () => {
    if (search_params.has('id')) {
        await listOfAllArticles();
        for (let kanap of listOfKanap) {
            let idList = kanap._id;
            let idArticle = search_params.get('id');
            if (idArticle === idList) {
                let nameKanap = kanap.name;
                let eltNameKanap = document.getElementById("title");
                eltNameKanap.textContent = `${nameKanap}`;
            } else {
                console.log(false);
            }
        }
    }
}

/**
 * Récupérer l'id de l'article correspondant à l'id récupérer dans l'url
 * @param {} kanap[].price
 * comparer l'id de l'article avec l'id de l'adresse url
 * Récupérer le prix de l'article de l'id de l'article correspondant
 * Intégrer le prix de l'article dans le DOM
 */
const priceArticle = async () => {
    if (search_params.has('id')) {
        await listOfAllArticles();
        for (let kanap of listOfKanap) {
            let idList = kanap._id;
            let idArticle = search_params.get('id');
            if (idArticle === idList) {
                let priceKanap = kanap.price;
                let eltPriceKanap = document.getElementById("price");
                eltPriceKanap.textContent = `${priceKanap}`;
            } else {
                console.log(false);
            }
        }
    }
}

/**
 * Récupérer l'id de l'article correspondant à l'id récupérer dans l'url
 * @param {} kanap[].description
 * comparer l'id de l'article avec l'id de l'adresse url
 * Récupérer le descriptif de l'article de l'id de l'article correspondant
 * Intégrer le descriptif de l'article dans le DOM
 */
const descriptionArticle = async () => {
    if (search_params.has('id')) {
        await listOfAllArticles();
        for (let kanap of listOfKanap) {
            let idList = kanap._id;
            let idArticle = search_params.get('id');
            if (idArticle === idList) {
                let descriptionKanap = kanap.description;
                let eltDescriptionKanap = document.getElementById("description");
                eltDescriptionKanap.textContent = `${descriptionKanap}`;
            } else {
                console.log(false);
            }
        }
    }
}

/**
 * Récupérer l'id de l'article correspondant à l'id récupérer dans l'url
 * @param {integer} kanap[].colors[]
 * Comparer l'id de l'article avec l'id de l'adresse url
 * Récupérer les couleurs de l'article de l'id de l'article correspondant
 * Intégrer les couleurs de l'article dans le DOM
 */
const colorArticle = async () => {
    if (search_params.has('id')) {
        await listOfAllArticles();
        for (let kanap of listOfKanap) {
            let idList = kanap._id;
            let idArticle = search_params.get('id');
            if (idArticle === idList) {
                let arrayColorsKanap = kanap.colors;
                for (let colors in arrayColorsKanap) {
                    let eltOptionColorsKanap = document.createElement('option');
                    let eltSelectColorsKanap = document.querySelector("select");
                    eltSelectColorsKanap.appendChild(eltOptionColorsKanap);
                    eltOptionColorsKanap.value = `${arrayColorsKanap[colors]}`;
                    console.log(eltOptionColorsKanap);
                    eltOptionColorsKanap.innerHTML = `${arrayColorsKanap[colors]}`;
                }
            } else {
                console.log(false);
            }
        }
    }
}



urlKanap();
listOfAllArticles();
imagArticle();
nameArticle();
priceArticle();
descriptionArticle();
colorArticle();


